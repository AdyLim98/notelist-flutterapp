import 'package:HttpRequest_Apps/models/api_response.dart';
import 'package:HttpRequest_Apps/models/note.dart';
import 'package:HttpRequest_Apps/note_createupdate.dart';
import 'package:HttpRequest_Apps/note_delete.dart';
import 'package:HttpRequest_Apps/services/notes_service.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';


class NoteList extends StatefulWidget {
  @override
  _NoteListState createState() => _NoteListState();
}

class _NoteListState extends State<NoteList> {
  // below may work ,but if the apps is big ,then u need to import 1 by 1 ,so we use GetIn package
  // final service = NotesService();
  //to use the GetIt package ,u need modify the main.dart ,steful widget
  NotesService get service => GetIt.I<NotesService>();
  // List <Note> notes = [];

  APIResponse <List<Note>> _apiResponse;
  bool _loading = false;

  String formatDateTime(DateTime dateTime){
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }

  @override
  void initState() {
    //pass the harcode data in the NoteSerivce.dart getNoteList into notes
    // notes = service.getNoteList();
    _fetchNotes();
    super.initState();
  }

  _fetchNotes() async{
    setState((){
      _loading=true;
    });

    _apiResponse = await service.getNoteList();

    setState((){
      _loading=false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        centerTitle: true,
        title:Text("Note List")
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (_) => NoteCreateUpdate()))
            .then((_){
              _fetchNotes();
            });
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
      ), 
      body:_loading?CircularProgressIndicator(): ListView.separated(
        //seperatorBuilder is the bottom line of each row
        separatorBuilder: (_, __) => Divider(height:1,color: Colors.blue),
        itemBuilder: (_, index){
          return Dismissible(
            key:ValueKey(_apiResponse.data[index].noteID),
            //swipe direction
            direction: DismissDirection.startToEnd,
            onDismissed: (direction){
              
            },
            confirmDismiss: (direction) async{
               final result = await showDialog(
                  context:context,
                  builder:(_) => NoteDeleteAlert()
                );
                var message;
                if(result == true){
                  final deleteResult = service.deleteNote(_apiResponse.data[index].noteID);
                  if(deleteResult != null && deleteResult == true){
                    message = "The note was deleted successfully";
                  }else{
                    message = "Error";
                  }
                   showDialog(
                      context:context,
                      builder:(_) => AlertDialog(
                        title:Text("Result"),
                        content:Text(message),
                        actions: [
                          FlatButton(
                            onPressed:(){
                              Navigator.of(context).pop();
                            }, 
                            child:Text("Ok")
                          )
                        ],
                      ) 
                    );

                return true;
              }

               print(result);
               return result;
            },
            //background for swipe left then will display the button
            background: Container(
              color:Colors.red,
              padding:EdgeInsets.only(left:16),
              child:Align(child:Icon(Icons.delete,color:Colors.white),alignment:Alignment.centerLeft)
            ),
            child:ListTile(
              title:Text(_apiResponse.data[index].noteTitle,style:TextStyle(color:Theme.of(context).primaryColor)),
              subtitle:Text(formatDateTime(_apiResponse.data[index].createdAt)),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (_) => NoteCreateUpdate(noteID: _apiResponse.data[index].noteID)))
                .then((_){
                  _fetchNotes();
                });
              },
          ));
        },
        itemCount: _apiResponse.data.length,
      ),
    );
  }
}