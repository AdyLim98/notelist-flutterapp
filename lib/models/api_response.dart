//<T> type here means will return back the same type data from the api
class APIResponse <T>{
  T data;
  bool error;
  String errorMessage;

  APIResponse({this.data,this.error=false ,this.errorMessage});
}